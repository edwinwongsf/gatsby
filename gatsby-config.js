module.exports = {
  pathPrefix: `/gatsby`,
  siteMetadata: {
    title: `Foo`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
